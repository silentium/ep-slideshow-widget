<?php
/*
Plugin Name: EP Slideshow Widget
Plugin URI: http://www.darkwhispering.com
Description: A image slideshow widget plugin based on Custom Post-type.
Author: Mattias Hedman
Author URI: http://www.darkwhispering.com
Version: 0.2.0
*/

define('EP_SLIDE_PLUGIN_VERSION','0.2.0');
add_action('init','ep_slide_plugin_version',1);
function ep_slide_plugin_version() {
	if (get_option('ep-slideshow-version') != EP_SLIDE_PLUGIN_VERSION) {
		update_option('eep-slideshow-old-version', get_option('ep-social-widget-version'));
		update_option('ep-slideshow-version',EP_SLIDE_PLUGIN_VERSION);
	}
}

class EP_slideshow
{
	private static $_textdomain = "ep-slideshow";

	function __construct()
	{
		// Path to the plugin directory
		$this->plugin_path = WP_PLUGIN_URL.DIRECTORY_SEPARATOR.str_replace(basename(__FILE__),"",plugin_basename(__FILE__));		
		
		// Default image sizes for the slideshow
		$this->image_sizes = array(
			'ep-slideshow-default' => array('width' => 600, 'height' => 400, 'crop' => true)
		);		
		add_option('ep-slideshow_image-sizes', $this->image_sizes);

		// Default slidshow to use
		add_option('ep-slideshow_script','epslide');

		//load_plugin_textdomain(self::$_textdomain, false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Load everything
		add_action('init', array(&$this, 'custom_post_types_and_taxonomies'));
		add_action('after_setup_theme', array(&$this, 'thumbnail_sizes'));
		add_action('admin_menu', array(&$this, 'add_settings_menu'));
		add_action('widgets_init', array(&$this, 'ep_widget_init'));
		add_action('wp_enqueue_scripts', array(&$this, 'scripts_and_styles'));
		add_action('admin_init', array(&$this, 'scripts_and_admin_styles'));		
	}
	
	function scripts_and_styles()
	{
		wp_enqueue_style("epslidestyle", $this->plugin_path."css".DIRECTORY_SEPARATOR."epslideshow.css" );
		wp_enqueue_script("jquery");

		$use_script = get_option('ep-slideshow_script');
		if ($use_script === 'cycle2') {
			wp_enqueue_script("epslide", $this->plugin_path."js".DIRECTORY_SEPARATOR."cycle2".DIRECTORY_SEPARATOR."jquery.cycle2.min.js", false, array(), true);
		} elseif ($use_script === 'epslide') {
			wp_enqueue_script("epslide", $this->plugin_path."js".DIRECTORY_SEPARATOR."epslide".DIRECTORY_SEPARATOR."jquery.epslideshow.js", false, array(), true);
		}
	}

	function scripts_and_admin_styles()
	{
		wp_register_style('epslideadminstyle', $this->plugin_path.'css'.DIRECTORY_SEPARATOR.'epslideshow_admin.css');
		wp_enqueue_style('epslideadminstyle');
		wp_register_script('epslideadminscript', $this->plugin_path.'js'.DIRECTORY_SEPARATOR.'epslideshow_admin.js');
		wp_enqueue_script('epslideadminscript');
	}

	function custom_post_types_and_taxonomies()
	{
		register_post_type( 'slide',
			array(
				'labels' => array(
					'name' 					=> __('Slides', self::$_textdomain),
					'all_items' 			=> __('Slides', self::$_textdomain),
					'singular_name' 		=> __('Slide', self::$_textdomain),
					'add_new' 				=> __('Add new slide', self::$_textdomain),
					'new_item' 				=> __('New slide', self::$_textdomain),
					'edit_item' 			=> __('Edit slide', self::$_textdomain),
					'view_item' 			=> __('View slide', self::$_textdomain),
					'search_items' 			=> __('Search slides', self::$_textdomain),
					'not_found' 			=> __('No slides found', self::$_textdomain),
					'not_found_in_trash' 	=> __('No slides found in Trash', self::$_textdomain),
					'parent_item_colon' 	=> __('Parent slide:', self::$_textdomain),
					'menu_name'				=> __('Slideshow', self::$_textdomain),
				),
				'hierarchical' 		=> true,
				'public' 			=> true,
				'menu_position' 	=> 20,
				'has_archive' 		=> false,
				'supports' 			=> array('title', 'editor', 'thumbnail', 'page-attributes'),
				'taxonomies' 		=> array('slideshow')
			)
		);

		register_taxonomy( 'slideshow', array('slide'/*, 'page', 'post' */),
			array(
				'hierarchical' 	=> true,
				'labels' 		=> array(
					'name' 			=> __('Slideshows', self::$_textdomain),
					'singular_name' => __('Slideshow', self::$_textdomain),
					'add_new_item' 	=> __('Add new slideshow', self::$_textdomain)
				),
				'show_ui' => true,
			)
		);


	}

	function thumbnail_sizes()
	{
		add_theme_support('post-thumbnails');
		
		$this->sizes = get_option('ep-slideshow_image-sizes');
		add_image_size("ep-slideshow-default", 600, 400, true);
		
		foreach ($this->sizes as $size => $opts) {
			add_image_size($size, $opts['width'], $opts['height'], $opts['crop']);
		}
	}
	
	function add_settings_menu()
	{
		add_submenu_page('edit.php?post_type=slide', __("Settings", self::$_textdomain), __("Settings", self::$_textdomain), 'manage_options', "slide-settings", array(&$this, 'showpage_manage_settings'));
	}
	
	function showpage_manage_settings()
	{
		include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'settings.php');
	}
	
	function ep_widget_init()
	{
		register_widget('ep_slideshow_wdgt');
	}
}

$ep_slideshow = new EP_slideshow();

class EP_slideshow_wdgt extends WP_Widget {
		private static $_textdomain = "ep-slideshow";

		function __construct() {
			$widget_opts = array('classname' => 'ep-slideshow', 'description' => __('A slideshow widget!',  self::$_textdomain));

			parent::WP_Widget('ep_slideshow_wdgt', 'EP Slideshow', $widget_opts);

		}

		function form($instance) {
			$defaults = array(
				'epslide' => array(
					'title' 		=> __('Slideshow', self::$_textdomain),
					'image_size' 	=> 'ep-slideshow-default',
					'fx' 			=> 'fade',
					'loop' 			=> true,
					'timeout' 		=> 2500,
					'speed' 		=> 600,
					'pager'			=> false
				),
				'cycle2' => array(
					'title' 		=> __('Slideshow', self::$_textdomain),
					'image_size' 	=> 'ep-slideshow-default',
					'fx'			=> 'fade',
					'loop'			=> true,
					'timeout'		=> 4000,
					'speed'			=> 500,
					'mouseover'		=> false,
					'pager'			=> false

				),
				'plain' => array(
					'title' 		=> __('Slideshow', self::$_textdomain),
					'image_size' 	=> 'ep-slideshow-default'
				)
			);
			$script = get_option('ep-slideshow_script');

			$instance = wp_parse_args((array) $instance, $defaults[$script]);
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', self::$_textdomain); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
			</p>
			<p>
			<?php
				$terms = get_terms('slideshow', array('hide_empty' => 0));
				$count = count($terms);
				if ($count > 0) :
				?>
					<label for="<?php echo $this->get_field_id( 'slideshow' ); ?>"><?php _e('Slideshow', self::$_textdomain); ?></label>
					<select class="widefat" id="<?php echo $this->get_field_id( 'slideshow' ); ?>" name="<?php echo $this->get_field_name( 'slideshow' ); ?>">
					<?php foreach ( $terms as $term ) : ?>
						<option <?php if ( $term->slug == $instance['slideshow'] ) echo 'selected="selected"'; ?> value="<?=$term->slug?>"><?=$term->name?> (<?=$term->count?>)</option>
					<?php endforeach; ?>
					</select>
				<?php
				endif;
			?>
			</p>

			<p>
				<?php $image_sizes = get_intermediate_image_sizes(); ?>

				<label for="<?php echo $this->get_field_id( 'image_size' ); ?>"><?php _e('Image size', self::$_textdomain); ?></label>
				
				<select class="widefat" id="<?php echo $this->get_field_id( 'image_size' ); ?>" name="<?php echo $this->get_field_name( 'image_size' ); ?>">
				  <?php foreach ($image_sizes as $size_name): ?>
				    <option <?php if ( $size_name == $instance['image_size'] ) echo 'selected="selected"'; ?> value="<?php echo $size_name ?>"><?php echo $size_name ?></option>
				  <?php endforeach; ?>
				</select>
			</p>

			<?php if($script === 'epslide') : ?>
				<p>
					<input class="checkbox" type="checkbox" <?php checked($instance['show_text'], 'on'); ?> id="<?php echo $this->get_field_id('show_text'); ?>" name="<?php echo $this->get_field_name('show_text'); ?>" />
					<label for="<?php echo $this->get_field_id('show_text'); ?>"><?php _e('Display text in slideshow', self::$_textdomain); ?></label>
				</p>
				<p>
					<input class="checkbox" type="checkbox" <?php checked($instance['loop'], 'on'); ?> id="<?php echo $this->get_field_id('loop'); ?>" name="<?php echo $this->get_field_name('loop'); ?>" />
					<label for="<?php echo $this->get_field_id('loop'); ?>"><?php _e('Loop slideshow', self::$_textdomain); ?></label>
				</p>
				<p>
					<input class="checkbox" type="checkbox" <?php checked($instance['pager'], 'on'); ?> id="<?php echo $this->get_field_id('pager'); ?>" name="<?php echo $this->get_field_name('pager'); ?>" />
					<label for="<?php echo $this->get_field_id('pager'); ?>"><?php _e('Show pager', self::$_textdomain); ?></label>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('timeout'); ?>"><?php _e('Time in ms to show each slide', self::$_textdomain); ?></label>
					<input type="text" name="<?php echo $this->get_field_name('timeout'); ?>" id="<?php echo $this->get_field_id('timeout'); ?>" size="3" value="<?php echo $instance['timeout']; ?>" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('speed'); ?>"><?php _e('Transition time in ms', self::$_textdomain); ?></label>
					<input type="text" name="<?php echo $this->get_field_name('speed'); ?>" id="<?php echo $this->get_field_id('speed'); ?>" size="3" value="<?php echo $instance['speed']; ?>" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('fx'); ?>">Transition:</label>
					<select id="<?php echo $this->get_field_id('fx'); ?>" name="<?php echo $this->get_field_name('fx'); ?>">
						<option <?php if ('fade' == $instance['fx']) echo 'selected="selected"'; ?>>fade</option>
						<option <?php if ('slide' == $instance['fx']) echo 'selected="selected"'; ?>>slide</option>
						<option <?php if ('off' == $instance['fx']) echo 'selected="selected"'; ?>>off</option>
					</select>
				</p>
			<?php elseif($script === 'cycle2') : ?>
				<p>
					<input class="checkbox" type="checkbox" <?php checked($instance['show_text'], 'on'); ?> id="<?php echo $this->get_field_id('show_text'); ?>" name="<?php echo $this->get_field_name('show_text'); ?>" />
					<label for="<?php echo $this->get_field_id('show_text'); ?>"><?php _e('Display text in slideshow', self::$_textdomain); ?></label>
				</p>
				<p>
					<input class="checkbox" type="checkbox" <?php checked($instance['loop'], 'on'); ?> id="<?php echo $this->get_field_id('loop'); ?>" name="<?php echo $this->get_field_name('loop'); ?>" />
					<label for="<?php echo $this->get_field_id('loop'); ?>"><?php _e('Loop slideshow', self::$_textdomain); ?></label>
				</p>
				<p>
					<input class="checkbox" type="checkbox" <?php checked($instance['mouseover'], 'on'); ?> id="<?php echo $this->get_field_id('mouseover'); ?>" name="<?php echo $this->get_field_name('mouseover'); ?>" />
					<label for="<?php echo $this->get_field_id('mouseover'); ?>"><?php _e('Pause on mouse over', self::$_textdomain); ?></label>
				</p>
				<p>
					<input class="checkbox" type="checkbox" <?php checked($instance['pager'], 'on'); ?> id="<?php echo $this->get_field_id('pager'); ?>" name="<?php echo $this->get_field_name('pager'); ?>" />
					<label for="<?php echo $this->get_field_id('pager'); ?>"><?php _e('Show pager', self::$_textdomain); ?></label>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('timeout'); ?>"><?php _e('Time in ms to show each slide', self::$_textdomain); ?></label>
					<input type="text" name="<?php echo $this->get_field_name('timeout'); ?>" id="<?php echo $this->get_field_id('timeout'); ?>" size="3" value="<?php echo $instance['timeout']; ?>" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('speed'); ?>"><?php _e('Transition time in ms', self::$_textdomain); ?></label>
					<input type="text" name="<?php echo $this->get_field_name('speed'); ?>" id="<?php echo $this->get_field_id('speed'); ?>" size="3" value="<?php echo $instance['speed']; ?>" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('fx'); ?>">Transition:</label>
					<select id="<?php echo $this->get_field_id('fx'); ?>" name="<?php echo $this->get_field_name('fx'); ?>">
						<option <?php if ('fade' == $instance['fx']) echo 'selected="selected"'; ?>>fade</option>
						<option <?php if ('fadeout' == $instance['fx']) echo 'selected="selected"'; ?>>fadeout</option>
						<option <?php if ('scrollHorz' == $instance['fx']) echo 'selected="selected"'; ?>>scrollHorz</option>
						<option <?php if ('none' == $instance['fx']) echo 'selected="selected"'; ?>>none</option>
					</select>
				</p>
			<?php
			endif;
		}

		function widget($args, $instance) {
			extract($args);
			$title = apply_filters('widget_title', $instance['title'] );
			echo $before_widget;
			if ( $title )
				echo $before_title . $title . $after_title;
			?>

			<?php include("slideshow_template.php"); ?>

			<?php
		echo $after_widget;

	}
}

/* Ajax Form Handler */
function eps_use_script() {
	if(!empty($_POST)) {
		update_option('ep-slideshow_script',$_POST['script']);
		echo json_encode(array('status' => 'success', 'data' => 'Changes saved'));
		die();
	} else {
		echo json_encode(array('status' => 'error', 'data' => 'Non valid data sent or no data sent'));
		die();
	}
}
add_action('wp_ajax_eps_use_script','eps_use_script');
	
function eps_size_form() {
	if(!empty($_POST)) {

		$sizes = get_option('ep-slideshow_image-sizes');

		if($_POST['eps_crop'] == 'false') {
			$_POST['eps_crop'] = 0;
		} elseif($_POST['eps_crop'] == 'true') {
			$_POST['eps_crop'] = 1;
		}

		if($_POST['eps_action'] === 'new') {
			$new_size = array($_POST['eps_size_name'] => array('width' => $_POST['eps_width'], 'height' => $_POST['eps_height'], 'crop' => $_POST['eps_crop']));
			$image_sizes = array_merge($sizes,$new_size);
		} elseif ($_POST['eps_action'] === 'edit') {
			foreach($sizes as $key => $size) {
				if($key == $_POST['eps_size_name']) {
					$image_sizes[$key]['width'] = $_POST['eps_width'];
					$image_sizes[$key]['height'] = $_POST['eps_height'];
					$image_sizes[$key]['crop'] = $_POST['eps_crop'];
				} else {
				 	$image_sizes[$key] = array('width' => $size['width'], 'height' => $size['height'], 'crop' => $size['crop']);
				}
			}
		}
		
		update_option('ep-slideshow_image-sizes', $image_sizes);

		$html = eps_html($image_sizes);

		echo $html;
		die();
	} else {
		echo 'Non valid data sent or no data sent';
		die();
	}
}
add_action('wp_ajax_eps_size_form','eps_size_form');


function eps_delete_size() {
	if(!empty($_POST)) {
		$image_sizes = get_option('ep-slideshow_image-sizes');

		unset($image_sizes[$_POST['eps_name']]);

		update_option('ep-slideshow_image-sizes', $image_sizes);

		$html = eps_html($image_sizes);

		echo $html;
		die();

	} else {
		echo 'Non valid data sent or no data sent';
		die();
	}
}
add_action('wp_ajax_eps_del_size','eps_delete_size');

function eps_html($image_sizes) {
	$alt = false;
	$html = '';
	foreach($image_sizes as $size => $opts) {
		$alt = !$alt;
		$html .= "<tr";
		if($alt) $html .= ' class="alternate"';
		$html .= ">";
			$html .= "<td>" . $size;
				$html .= '<div class="row-actions">';
					$html .= '<span class="edit">';
						$html .= '<a href="#" data-eps-name="'.$size.'"  data-eps-width="'.$opts['width'].'" data-eps-height="'.$opts['height'].'" data-eps-crop="'.$opts['crop'].'" >Edit</a> | ';
					$html .= '</span>';
					$html .= '<span class="delete">';
						$html .= '<a class="submitdelete" href="#" data-eps-name="'.$size.'">Delete</a>';
					$html .= '</span>';
				$html .= '</div>';
			$html .= "</td>\n\t";
			$html .= "<td>" . $opts['width'] . "</td>\n\t";
			$html .= "<td>" . $opts['height'] . "</td>\n\t";
			$html .= "<td>";
			$html .= $opts['crop'] ? 'Yes' : 'No';
			$html .= "</td>\n";
		$html .= "</tr>";
	}

	return $html;
}







