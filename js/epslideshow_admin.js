jQuery(document).ready(function($) {
	// Clicking the "add new size" link
	$('#eps_nav').on('click','#eps_add_new',function(e){
		e.preventDefault();

		$('.eps-size-form input#eps_height').val('200');
		$('.eps-size-form input#eps_width').val('400');
		$('.eps-size-form input#eps_crop_true').attr('checked',true);
		$('.eps-size-form input#eps_size_name').val('ep-slideshow-small');
		$('.eps-size-form input#eps_action').val('new');

		if($('.eps-size-form').is(':visible')) {
			$('.eps-size-form').slideUp('fast');
		} else {
			$('.eps-size-form').slideDown('fast');
		}
	});

	// Clicking "edit" for a image size
	$('#the-list').on('click','.edit a',function(e){
		e.preventDefault();

		if($(this).data().epsCrop == 1) {
			$('.eps-size-form input#eps_crop_true').attr('checked',true);
		} else {
			$('.eps-size-form input#eps_crop_false').attr('checked',true);
		}
		
		$('.eps-size-form input#eps_height').val($(this).data().epsHeight);
		$('.eps-size-form input#eps_width').val($(this).data().epsWidth);
		$('.eps-size-form input#eps_size_name').val($(this).data().epsName);
		$('.eps-size-form input#eps_action').val('edit');
		$('.eps-size-form').slideDown();
	});

	// Clicking on "delete" for an image size
	$('#the-list').on('click','.delete a',function(e){
		e.preventDefault();

		var action = confirm("Do you really want to delete this item?");
		if(action){
			var formdata = $(this).data().epsName;
			$.ajax({
				url : '/wp-admin/admin-ajax.php',
				type : 'POST',
				data : 'action=eps_del_size&eps_name='+formdata,
				dataType : 'HTML',
				success : function(response){
					$('.eps-size-form').fadeOut();
					$('#the-list').html(response);
					$('#eps-script-msg').html('Image size deleted').fadeIn().delay(1000).fadeOut();
				}
			});
		}
	});

	// Clicing "cancel" in new size form
	$('.eps-size-form').on('click','#eps_cancel',function(){
		$('.eps-size-form').slideUp('fast');
	});

	// Submitting "new size" form
	$('.eps-size-form form').on('submit',function(e){
		e.preventDefault();

		var formdata = $(this).serialize();
		$.ajax({
			url : '/wp-admin/admin-ajax.php',
			type : 'POST',
			data : 'action=eps_size_form&'+formdata,
			dataType : 'HTML',
			success : function(response){
				$('.eps-size-form').slideUp();
				$('#the-list').html(response);
				$('#eps-script-msg').html('Image size saved').fadeIn().delay(1000).fadeOut();
			}
		});
	});

	// SLider plugin to use
	$('#ep-slideshow-script form').on('change',function(e){
		e.preventDefault();

		var formdata = $(this).serialize();
		$.ajax({
			url : '/wp-admin/admin-ajax.php',
			type : 'POST',
			data : 'action=eps_use_script&'+formdata,
			dataType : 'JSON',
			success : function(response) {
				if(response.status == 'success') {
					$('#eps-script-msg').html(response.data).fadeIn().delay(1000).fadeOut();
				} else if(response.status == 'error') {
					alert(response.data);
				}
			}
		});
	});
});