(function($){
	$.fn.epslideshow = function(opts) {
		return this.each(function(i, v) {
			var $this = $(this),
				_this = this;
			// override default settings
			var settings = $.extend(true, {}, $.fn.epslideshow.defaults, opts, $this.data("options"));
			
			// if no width or height, try to figure it out anyway
			settings.width = settings.width || $(settings.item, $this).first().find("img").width();
			settings.height = settings.height || $(settings.item, $this).first().find('img').height();

			// unique slideshow id, if not set in settings
			settings.id = settings.custom_id || 'epslideshow_' + i;
			
			// count the slides and add classes.
			var slidecount = $(settings.item, $this).addClass(function(index) {
				return "epslideshow-item-" + index;
			}).hide().css({
				width: settings.width,
				height: settings.height,
				position: "absolute",
				top: 0,
				left: 0
			}).length,
			max = slidecount-1,
			current = 0,
			next = 1,
			prev = settings.loop && max || -1,
			$pager,
			$next = settings.next.clone(),
			$prev = settings.prev.clone(),
			timer;
			
			settings.timeout = parseInt(settings.timeout, 10);
			
			
			$this.css({
				position: 'relative'
			});			
				
			// put everything in wrappers
			var $wrap = settings.wrap.attr('id', settings.id).addClass("epslideshow-wrapper").css({
				position: 'relative'
			}).width(settings.width).height(settings.height);
			
			var $inner_wrap = $('<div />').addClass("epslideshow-inner-wrap").css({
				overflow: 'hidden',
				height: settings.height,
				width: settings.width
			});

			$this.wrap($wrap).wrap($inner_wrap);
			
			if(settings.pager) {
				$pager = $('<div />').addClass('epslideshow-pager').css({

				});
				$pager.prepend($next).prepend('<span class="epslideshow-current">1 / ' + slidecount + '</span>').prepend($prev);
				$pager.appendTo($("#" + settings.id));
			}
			
			$(settings.item, $this).first().show();
			

			
			
			var change_slide = function(e) {
				var show = (e.type === 'nextSlide') ? next : prev;
				
				switch(settings.fx) {
					case "slide":
						var dir = (current<show) ? "-=" : "+=";		
						if(typeof settings.callback.before === 'function') {
							settings.callback.before(current);
						}
						$(settings.item, $this).eq(current).animate({
							left: dir + settings.width
						}, settings.speed, settings.easing);
						$(settings.item, $this).eq(show).show().css({
							left: (current<show) ? settings.width : -settings.width
						}).animate({
							left: 0
						}, settings.speed, settings.easing, function() {
							if(typeof settings.callback.after === 'function') {
								settings.callback.after(current);
							}
						});
					break;
					case "fade":
						if(typeof settings.callback.before === 'function') {
							settings.callback.before(current);
						}
						$(settings.item, $this).eq(current).fadeOut(settings.speed);
						$(settings.item, $this).eq(show).fadeIn(settings.speed, function() {
							if(typeof settings.callback.after === 'function') {
								settings.callback.after(current);
							}
						});
					break;
					default:
						if(typeof settings.callback.before === 'function') {
							settings.callback.before(current);
						}
						$(settings.item, $this).eq(current).hide();
						$(settings.item, $this).eq(show).show();
						if(typeof settings.callback.after === 'function') {
							settings.callback.after(current);
						}
					break;
				}
			
				
				// update current, next and previous
				current = show;
				next = current+1;
				prev = current-1;
				


				if(next>max && settings.loop) {
					next = 0;
				}
				if(next>max && !settings.loop) {
					$next.hide();
					$this.unbind('nextSlide', change_slide);
				} else {
					$next.show();
					// FIXME: only rebind if unbound
					$this.unbind('nextSlide', change_slide).bind('nextSlide', change_slide);
				}

				if(prev<0 && settings.loop) {
					prev = max;
				}
				if(prev<0 && !settings.loop) {
					$prev.hide();
					$this.unbind('prevSlide', change_slide);
					
				} else {
					$prev.show();
					// FIXME: only rebind if unbound
					$this.unbind('prevSlide', change_slide).bind('prevSlide', change_slide);
					
				}
				
				$(".epslideshow-current", $pager).text('' + parseInt(current+1, 10) + ' / ' + slidecount);
				
			};
			
			$this.bind('nextSlide', change_slide);
			
			$this.bind('prevSlide', change_slide);
			
			if(!settings.loop) {
				$prev.hide();
				$this.unbind('prevSlide', change_slide);
			}
			
			$next.bind("click", function(e) {
				e.preventDefault();
				$this.trigger('nextSlide');
			});
			$prev.bind("click", function(e) {
				e.preventDefault();
				$this.trigger('prevSlide');
			});
			
			timer = {
				start: function() {
					timer.t = setInterval(function() {
						$this.trigger('nextSlide');
					}, settings.timeout);
				},
				pause: function() {
					clearInterval(timer.t);
				},
				restart: function() {
					timer.pause();
					timer.start();
				},
				t: 0
			};
			
			if(settings.timeout) {
				timer.start();
			}
			
			if(settings.pause_on_hover && settings.timeout) {
				$("#" + settings.id).bind("mouseenter mouseleave", function(e) {
					if(e.type==='mouseleave') {
						timer.start();
					} else if(e.type==='mouseenter') {
						timer.pause();
					}
				});
			}
			
			// click on slide to navigate
			$this.bind("click", function(e) {
				if(e.offsetX>settings.width/2) {
					$this.trigger("nextSlide");
				} else {
					$this.trigger("prevSlide");
				}
			});
			

			
			
		});
	};
	
	$.fn.epslideshow.defaults = {
		width: null,
		height: null,
		item: "li", // each slide item, selector
		pager: false, // true or false
		next: $("<a />").attr('href', '#').addClass("next").text('»'), // jQuery obj
		prev: $("<a />").attr('href', '#').addClass("prev").text('«'), // jQuery obj
		wrap: $('<div />'),
		loop: false,
		timeout: 0, // auto advance every X seconds, 0 to disable
		pause_on_hover: true,
		fx: 'fade', // fade or slide
		speed: 600,
		custom_id: null,
		easing: 'swing', // swing or linear
		callback: {
			after: null, // function, param: current slide
			before: null // function, param: current slide
		}
	};
	
})(jQuery);

// document ready
jQuery(document).ready(function($) {
	$("ul.ep-slideshow").epslideshow();
});
