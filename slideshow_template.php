<?php 
$slide_query = new WP_Query(array(
	'slideshow' 		=> $instance['slideshow'],
	'post_type' 		=> 'slide',
	'posts_per_page' 	=> -1,
	'orderby' 			=> 'menu_order',
	'order' 			=> 'ASC'
	)
);
$eps_script = get_option('ep-slideshow_script');
?>


<?php if ($eps_script === 'plain') : ?>
<ul class="ep-slideshow eps-list">
<?php elseif ($eps_script === 'epslide') : ?>
<ul class="ep-slideshow" data-options='<?php echo json_encode($instance); ?>'>
<?php elseif ($eps_script === 'cycle2') : ?>
<ul class="ep-slideshow"
	data-cycle-fx="<?php echo $instance['fx']; ?>"
	data-cycle-loop="<?php echo $instance['loop'] === 'on' ? 0 : 1; ?>"
	data-cycle-pause-on-hover="<?php echo $instance['mouseover'] === 'on' ? true : false; ?>"
	data-cycle-speed="<?php echo $instance['speed']; ?>"
	data-cycle-timeout="<?php echo $instance['timeout']; ?>"
	data-cycle-slides="> .eps-listitem"
	data-cycle-pager=".cycle-nav > .cycle-pager"
	data-cycle-prev=".cycle-prev"
     data-cycle-next=".cycle-next"
	>
<?php endif; ?>

<?php while ($slide_query->have_posts()) : $slide_query->the_post(); ?>
	<li class="eps-listitem">	
	<?php if (has_post_thumbnail()) : ?>
		  <?php the_post_thumbnail($instance["image_size"]); ?>
	<?php endif; ?>

	<?php if (isset($instance["show_text"])) : ?>
		<div class="ep-slideshow-caption">
			<h2><?php the_title() ?></h2>
			<?php the_content(); ?>
		</div>
	<?php endif; ?>
	</li>
<?php endwhile; ?>

<?php if (isset($instance['pager'])) : ?>
	<div class="cycle-nav">
		<div class="cycle-prev"><?php echo __('< Prev'); ?></div>
		<div class="cycle-pager"></div>
	    	<div class="cycle-next"><?php echo __('Next >'); ?></div>
    	</div>
<?php endif; ?>

</ul>
