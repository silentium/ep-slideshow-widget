<div class="wrap">
	<div id="icon-options-general" class="icon32"><br /></div><h2><?php echo __('Image sizes'); ?></h2>

	<ul class='subsubsub' id="eps_nav">
		<li><a href="#add new" id="eps_add_new"><?php echo __('Add new size'); ?></a></li>
	</ul>

	<div class="eps-size-form size-form">

		<h2><?php echo __('Add new size'); ?></h2>

		<form>
			<table class="form-table">
	            <tbody>
	               	<tr valign="top">
	                   	<th scope="row">
	                       	<label for="eps_size_name"><?php echo __('Name'); ?></label>
	                   	</th>
	                   	<td>
	                       	<input type="text" class="regular-text" name="eps_size_name" id="eps_size_name" />
	                   	</td>
	               	</tr>
	               	<tr valign="top">
	                   	<th scope="row">
	                       	<label for="eps_width"><?php echo __('Width'); ?></label>
	                   	</th>
	                   	<td>
	                       	<input type="text" class="small-text" name="eps_width" id="eps_width" />
	                   	</td>
	               	</tr>
	               	<tr valign="top">
	                   	<th scope="row">
	                       <label for="eps_width"><?php echo __('Height'); ?></label>
	                   	</th>
	                   	<td>
	                       	<input type="text" class="small-text" name="eps_height" id="eps_height" />
	                   	</td>
	               	</tr>
	               	<tr valign="top">
	                   	<th scope="row">
	                       	<?php echo __('Crop'); ?>
	                   	</th>
	                   	<td>
	                   		<input type="radio" name="eps_crop" id="eps_crop_false" value="false" /> <?php echo __('No'); ?> &nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" name="eps_crop" id="eps_crop_true" value="true"/> <?php echo __('Yes'); ?>
	                   	</td>
	               	</tr>
	           	</tbody>
	      	</table>
	      	<p class="submit">
	      		<input type="hidden" name="eps_action" value="new" />
	     		<input type="submit" class="button-primary" id="save" name="eps_submit" value="<?php echo __('Save'); ?>"/> <button type="button" id="eps_cancel"><?php echo __('Cancel'); ?></button>
	     	</p>
    	</form>

	</div>

	<table class="wp-list-table widefat" cellspacing="0">
		<thead>
			<tr>
				<th class="manage-column column-title"><?php _e("Name", self::$_textdomain); ?></th>
				<th class="manage-column column-title"><?php _e("Width", self::$_textdomain); ?></th>
				<th class="manage-column column-title"><?php _e("Height", self::$_textdomain); ?></th>
				<th class="manage-column column-title"><?php _e("Crop", self::$_textdomain); ?></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th class="manage-column column-title"><?php _e("Name", self::$_textdomain); ?></th>
				<th class="manage-column column-title"><?php _e("Width", self::$_textdomain); ?></th>
				<th class="manage-column column-title"><?php _e("Height", self::$_textdomain); ?></th>
				<th class="manage-column column-title"><?php _e("Crop", self::$_textdomain); ?></th>
			</tr>
		</tfoot>

		<tbody id="the-list">
		<?php
			$this->sizes = get_option('ep-slideshow_image-sizes');

			$alt = false;
			foreach ($this->sizes as $size => $opts) {
				$alt = !$alt;
				echo "<tr";
				if ($alt) echo ' class="alternate"';
				echo ">";
					echo "<td>" . $size;
					?>
						<div class="row-actions">
							<span class="edit">
								<a href="#" data-eps-name="<?php echo $size; ?>"  data-eps-width="<?php echo $opts['width']; ?>" data-eps-height="<?php echo $opts['height']; ?>" data-eps-crop="<?php echo $opts['crop']; ?>"><?php echo __('Edit'); ?></a> | 
							</span>
							<span class="delete">
								<a class="submitdelete" href="#" data-eps-name="<?php echo $size; ?>"><?php echo __('Delete'); ?></a>
							</span>
						</div>	
					<?php
					echo "</td>\n\t";
					echo "<td>" . $opts['width'] . "</td>\n\t";
					echo "<td>" . $opts['height'] . "</td>\n\t";
					echo "<td>" . ($opts['crop'] ? __("Yes", self::$_textdomain) : __("No", self::$_textdomain)). "</td>\n";
				echo "</tr>";
			}
		?>
		</tbody>
	</table>

	<div id="ep-slideshow-script">
		<?php $script = get_option('ep-slideshow_script'); ?>
		<h2><?php echo __('Choose slider plugin to use'); ?></h2>
		<form method="post">
			<p>
				<label for="epslide"><input id="epslide" type="radio" value="epslide" name="script" <?php echo $script === 'epslide' ? 'checked="checked"' : ''; ?>> EP Slider</label>
				<span class="description"> - Simple slider developer by Earth People AB</span>
			</p>
			<p>
				<label for="cycle2"><input id="cycle2" type="radio" value="cycle2" name="script" <?php echo $script === 'cycle2' ? 'checked="checked"' : ''; ?>> Cycle 2</label>
				<span class="description"> - Advanced slider plugin while options for almost everything</span>
			</p>
			<p>
				<label for="plain"><input id="plain" type="radio" value="plain" name="script" <?php echo $script === 'plain' ? 'checked="checked"' : ''; ?>> <?php echo __('Plain list'); ?></label>
				<span class="description"> - Output plain HTML code. Use this when you want to use your own slider or another slider plugin</span>
			</p>
		</form>
		<div class="updated" id="eps-script-msg" style="display:none;"></div>
	</div>	

</div>


<?php


?>








